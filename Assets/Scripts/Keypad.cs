using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Keypad : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private string correctAnswer = "3199";
    [SerializeField] private Animator doorsAnim;
    [SerializeField] private TextMeshProUGUI tmp;
    [SerializeField] private MeshRenderer screenRenderer;

    [Header("Audio")]
    [SerializeField] private AudioSource correctSound;
    [SerializeField] private AudioSource wrongSound;
    [SerializeField] private AudioSource buttonPressedSound;

    private string answer = "";
    private bool isOpen = false;
    private bool canSelect = true;

    private void Start()
    {
        tmp.text = answer;
    }

    public void SelectNumber(int number)
    {
        if(isOpen || !canSelect) { return; }

        buttonPressedSound.Play();
        answer += number.ToString();
        tmp.text = answer;
        CheckAnswer();
    }

    private void CheckAnswer()
    {
        if (answer != correctAnswer)
        {
            if (answer.ToCharArray().Length != 0 && answer.ToCharArray().Length == 4)
            {
                StartCoroutine(DoWrong());
                return;
            }
            return;
        }
        else
        {
            DoCorrect();
        }
    }

    private void DoCorrect()
    {
        correctSound.Play();
        doorsAnim.SetBool("isOpen", true);
    }

    private IEnumerator DoWrong()
    {
        canSelect = false;
        wrongSound.Play();

        //Change color of screen and text
        Color baseColor = screenRenderer.material.color;
        Color baseTextColor = tmp.color;
        screenRenderer.material.color = Color.red;
        tmp.color = Color.red;

        yield return new WaitForSeconds(0.5f);

        //Back to basic color and text reset for new try
        screenRenderer.material.color = baseColor;
        tmp.color = baseTextColor;
        answer = "";
        tmp.text = answer;

        canSelect = true;
    }

    public void AutoDestruct()
    {
        doorsAnim.SetBool("isOpen", false);
        Destroy(gameObject);
    }
}
