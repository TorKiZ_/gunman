using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTrigger : MonoBehaviour
{
    [SerializeField] private Keypad keypad;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(keypad != null)
            {
                keypad.AutoDestruct();
            }
            Destroy(gameObject);
        }
    }
}
