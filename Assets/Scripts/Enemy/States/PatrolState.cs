using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : BaseState
{
    private int waypointIndex;
    private float rotationSpeed = 5f;

    public override void Enter()
    {
    }

    public override void Perform()
    {
        PatrolCycle();
    }

    public override void Exit()
    {
    }

    public void PatrolCycle()
    {
        if (enemy.transform.position.x == enemy.path.waypoints[waypointIndex].position.x)
        {
            if (enemy.transform.position.z == enemy.path.waypoints[waypointIndex].position.z)
            {
                if (waypointIndex == enemy.path.waypoints.Count - 1)
                {
                    waypointIndex = 0;
                }
                else
                {
                    waypointIndex++;
                }
            }
        }
        enemy.Agent.SetDestination(enemy.path.waypoints[waypointIndex].position);
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, Quaternion.LookRotation(enemy.path.waypoints[waypointIndex == enemy.path.waypoints.Count ? 0 : waypointIndex].position - enemy.transform.position), rotationSpeed * Time.deltaTime);
    }
}
